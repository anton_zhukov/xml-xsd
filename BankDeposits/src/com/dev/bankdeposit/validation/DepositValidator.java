package com.dev.bankdeposit.validation;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class DepositValidator {

    private static final Logger LOG = Logger.getLogger(DepositValidator.class);

    private static final String FILE_NAME = "BankDeposits\\banks.xml";
    private static final String SCHEMA_NAME = "BankDeposits\\banks.xsd";
    private static final String LANGUAGE = XMLConstants.W3C_XML_SCHEMA_NS_URI;

    public static void validateFile() {
        SchemaFactory factory = SchemaFactory.newInstance(LANGUAGE);
        File schemaLocation = new File(SCHEMA_NAME);
        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(FILE_NAME);
            validator.validate(source);
            LOG.info(FILE_NAME + " is valid.");
        } catch (SAXException e) {
            LOG.error("validation " + FILE_NAME + " is not valid because "
                    + e.getMessage());
        } catch (IOException e) {
            LOG.error(FILE_NAME + " is not valid because "
                    + e.getMessage());
        }
    }
}