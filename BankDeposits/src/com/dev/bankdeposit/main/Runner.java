package com.dev.bankdeposit.main;

import com.dev.bankdeposit.validation.DepositValidator;

public class Runner {
    public static void main(String[] args) {
        DepositValidator.validateFile();
    }
}
